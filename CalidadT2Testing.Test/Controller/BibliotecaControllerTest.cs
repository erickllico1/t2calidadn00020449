﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2Testing.Test.Controller
{
    
    [TestFixture]
    public class BibliotecaControllerTest
    {
        [Test]
        public void CasoBiblioteca()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.ObtenerDatosBiblioteca()).Returns(new List<Biblioteca>());
            var controller = new BibliotecaController(claim.Object);
            var view = controller.Index() as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }
        /*[Test]
        public void CasoAddComentary()
        {
            var repository = new Mock<IBookRepository>();

            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.AddComentario(new Comentario { Texto = "abc" })).Returns(new Comentario());
            var controller = new LibroController(repository.Object, claim.Object);
            var view = controller.AddComentario(new Comentario { Texto = "abc" }) as ViewResult;

            Assert.AreEqual("Details", view.ViewName);
        }
        */
    }
}
